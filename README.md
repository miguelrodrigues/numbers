# Numbers

A small gem to translate integers (between -999..999) into natural language.
At the moment only English and Portuguese are available.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'numbers'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install numbers

## Usage

    $ EnglishNumber.new(some_number)
    $ EnglishNumber.new(some_number).in_english
    $ EnglishNumber.new(some_number).in_portuguese

## Examples
    $ EnglishNumber.new(6).in_english # => 'six'
    $ EnglishNumber.new(11).in_english # => 'eleven'
    $ EnglishNumber.new(22).in_english # => 'twenty-two'
    $ EnglishNumber.new(24.5).in_english # => 'twenty-four'
    $ EnglishNumber.new(222).in_english # => 'two hundred and twenty-two'
    $ EnglishNumber.new(1001).in_english # => nil

    $ EnglishNumber.new(6).in_portuguese # => 'seis'
    $ EnglishNumber.new(11).in_portuguese # => 'onze'
    $ EnglishNumber.new(22).in_portuguese # => 'vinte e dois'
    $ EnglishNumber.new(24.5).in_portuguese # => 'vinte e quatro'
    $ EnglishNumber.new(222).in_portuguese # => 'duzentos e vinte e dois'
    $ EnglishNumber.new(1001).in_portuguese # => nil


## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://miguelrodrigues@bitbucket.org/miguelrodrigues/numbers.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).