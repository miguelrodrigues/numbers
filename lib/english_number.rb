require "number_translations"

class EnglishNumber
  include NumberTranslations

  def initialize(number)
    return nil unless number.is_a? Numeric
    @number = number
    set_integer_digits
  end

  def in_english
    translate
  end

  def in_portuguese
    translate("pt")
  end

  private

    def translate(locale = "en")
      return nil unless @number
      return nil unless @number.between?(-999, 999)
      return nil unless locale == "en" || locale == "pt"

      return WORDS[locale][:ONE_TO_NINETEEN][0] if @number == 0

      number_name = ''
      @number_to_translate = @number

      # is negative number ?
      if @number_to_translate < 0
        number_name += WORDS[locale][:NEGATIVE] + ' '
        @number_to_translate = @number_to_translate.abs
      end

      @number_to_translate = @number_to_translate.floor

      number_name += translate_sub_one_thousand(locale)
    end

    def translate_sub_one_thousand(locale)
      number_name = ''

      if @hundreds
        if locale == "en"
          number_name += WORDS[locale][:ONE_TO_NINETEEN][@hundreds]
          number_name += " "
          number_name += WORDS[locale][:HUNDRED]
        elsif locale == "pt"
          if @number_to_translate == 100
            number_name += WORDS[locale][:HUNDRED][0]
          else
            number_name += WORDS[locale][:HUNDRED][@hundreds]
          end
        end

        if (@dozens != 0 || @units != 0)
          number_name += WORDS[locale][:HUNDREDS_SEPARATOR]
        end

        @number_to_translate = "#{@dozens}#{@units}".to_i
      end

      if @number_to_translate > 0
        number_name += translate_sub_one_hundred(locale)
      end
      
      number_name
    end

    def translate_sub_one_hundred(locale)
      number_name = ''

      if @number_to_translate < 20
        number_name += WORDS[locale][:ONE_TO_NINETEEN][@number_to_translate]
      else
        if @units == 0
          number_name += WORDS[locale][:"#{@dozens}"]
        else
          number_name += WORDS[locale][:"#{@dozens}"] + WORDS[locale][:DOZENS_SEPARATOR] + WORDS[locale][:ONE_TO_NINETEEN][@units]
        end
      end
    end

    def set_integer_digits
      i = @number.abs.floor
      arr = []

      while i > 0
        digit = i % 10
        i /= 10
        arr << digit  
      end

      @units = arr[0] unless arr[0].nil?
      @dozens = arr[1] unless arr[1].nil?
      @hundreds = arr[2] unless arr[2].nil?
    end
end
