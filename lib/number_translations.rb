module NumberTranslations
  WORDS = {
            "en" => {
                :NEGATIVE => "negative",
                :HUNDRED => "hundred",
                :DOZENS_SEPARATOR => "-",
                :HUNDREDS_SEPARATOR => " and ",
                :ONE_TO_NINETEEN => [ 'zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'],
                :'2' => "twenty",
                :'3' => "thirty",
                :'4' => "forty",
                :'5' => "fifty",
                :'6' => "sixty",
                :'7' => "seventy",
                :'8' => "eighty",
                :'9' => "ninety"
            },
            "pt" => {
                :NEGATIVE => "menos",
                :DOZENS_SEPARATOR => " e ",
                :HUNDREDS_SEPARATOR => " e ",
                :ONE_TO_NINETEEN => [ 'zero', 'um', 'dois', 'tres', 'quatro', 'cinco', 'seis', 'sete', 'oito', 'nove', 'dez', 'onze', 'doze', 'treze', 'catorze', 'quinze', 'dezasseis', 'dezassete', 'dezoito', 'dezanove'],
                :'2'    => "vinte",
                :'3'    => "trinta",
                :'4'    => "quarenta",
                :'5'    => "cinquenta",
                :'6'    => "sessenta",
                :'7'    => "setenta",
                :'8'    => "oitenta",
                :'9'    => "noventa",
                :HUNDRED => ["cem", "cento", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"]
            }
          }
end