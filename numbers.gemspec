# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'numbers/version'

Gem::Specification.new do |spec|
  spec.name          = "numbers"
  spec.version       = Numbers::VERSION
  spec.authors       = ["Miguel Rodrigues"]
  spec.email         = ["miguel.srod@gmail.com"]

  spec.summary       = %q{Translate integers into natural language.}
  spec.description   = %q{Translate integers into natural language. At the moment only available in english and portuguese, for numbers between [-999, 999].}
  spec.homepage      = "https://bitbucket.org/miguelrodrigues/numbers"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.13"
  spec.add_development_dependency "rake", "~> 11.3"
  spec.add_development_dependency "rspec", "~> 3.2"
end
