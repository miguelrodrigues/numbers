ENGLISH_TESTS_SUB_ONE_HUNDRED = [
 [-1,     "negative one"],
 [1,      "one"],
 [0,      "zero"],
 [6,      "six"],
 [11,     "eleven"],
 [24.15,  "twenty-four"],
 [24,     "twenty-four"],
 [24.5,   "twenty-four"],
 [99,     "ninety-nine"],
 [-99,    "negative ninety-nine"]
]

ENGLISH_TESTS_SUB_ONE_THOUSAND = [
 [100,    "one hundred"],
 [-100,   "negative one hundred"],
 [106,    "one hundred and six"],
 [111,    "one hundred and eleven"],
 [124.15, "one hundred and twenty-four"],
 [124,    "one hundred and twenty-four"],
 [124.5,  "one hundred and twenty-four"],
 [456.15, "four hundred and fifty-six"],
 [456,    "four hundred and fifty-six"],
 [456.5,  "four hundred and fifty-six"],
 [500,    "five hundred"],
 [-500,   "negative five hundred"],
 [999,    "nine hundred and ninety-nine"],
 [-999,   "negative nine hundred and ninety-nine"],
 [1000,   nil]
]

PORTUGUESE_TESTS_SUB_ONE_HUNDRED = [
 [-1,     "menos um"],
 [1,      "um"],
 [0,      "zero"],
 [6,      "seis"],
 [11,     "onze"],
 [24.15,  "vinte e quatro"],
 [24,     "vinte e quatro"],
 [24.5,   "vinte e quatro"],
 [99,     "noventa e nove"],
 [-99,    "menos noventa e nove"]
]

PORTUGUESE_TESTS_SUB_ONE_THOUSAND = [
 [100,    "cem"],
 [-100,   "menos cem"],
 [106,    "cento e seis"],
 [111,    "cento e onze"],
 [124.15, "cento e vinte e quatro"],
 [124,    "cento e vinte e quatro"],
 [124.5,  "cento e vinte e quatro"],
 [456.15, "quatrocentos e cinquenta e seis"],
 [456,    "quatrocentos e cinquenta e seis"],
 [456.5,  "quatrocentos e cinquenta e seis"],
 [500,    "quinhentos"],
 [-500,   "menos quinhentos"],
 [999,    "novecentos e noventa e nove"],
 [-999,   "menos novecentos e noventa e nove"],
 [1000,   nil]
]
