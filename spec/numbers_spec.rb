require "spec_helper"
require "tests"

describe Numbers do
  it "has a version number" do
    expect(Numbers::VERSION).not_to be nil
  end

  it "EnglishNumber requires a number to be passed to a new object" do
    expect { EnglishNumber.new }.to raise_error ArgumentError
  end
end

describe "has support for english translations" do

  ENGLISH_TESTS_SUB_ONE_HUNDRED.each do |num, human|
    it "#{num} is #{human}" do
      expect(EnglishNumber.new(num).in_english).to eql(human)
    end
  end

  ENGLISH_TESTS_SUB_ONE_THOUSAND.each do |num, human|
    it "#{num} is #{human}" do
      expect(EnglishNumber.new(num).in_english).to eql(human)
    end
  end
end

describe "has support for portuguese translations" do

  PORTUGUESE_TESTS_SUB_ONE_HUNDRED.each do |num, human|
    it "#{num} is #{human}" do
      expect(EnglishNumber.new(num).in_portuguese).to eql(human)
    end
  end

  PORTUGUESE_TESTS_SUB_ONE_THOUSAND.each do |num, human|
    it "#{num} is #{human}" do
      expect(EnglishNumber.new(num).in_portuguese).to eql(human)
    end
  end
end
